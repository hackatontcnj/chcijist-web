<?php

namespace Recipes\Controllers;

use GuzzleHttp\Client;

class RecipesController {

  private static $urlProductByIngredient = "http://chcijist-api.azurewebsites.net/api/find-product?code=zPViHSkM4aUo2GoMjf1UyuORAmarW8LEbxR2%2FydQEaK54am6Z92qcw%3D%3D&name=%ingredient%";
  private static $urlRecipe = "http://chcijist-api.azurewebsites.net/api/get-recipe?code=AU8SzHHionqkcJulmstoFa3xY6GVdH5aDBiQ3CLz4voaf0GxlPIFWw==&id=%id%";
  private static $urlList = "http://chcijist-api.azurewebsites.net/api/list-recipes?code=oB63a9jaIi0VC3KkuyHhCOWiYq41Xj3M31YestjvD5nupnh1TVMjzA==";
  private static $urlScan = "http://chcijist-api.azurewebsites.net/api/ocr-get-ingrediencies?code=Qbvx7DUN/F4QBfkBNqsgXBcCsHsya3FfcgA3H0bUHQa2aZSU54VrGg==&url=https://lh3.googleusercontent.com/-DoMllSbJ1AdhsdGGsJz3U0zyzMuG0nQkb59pRaOZRy_pWA6bF_KYhOnsxIzfs8c5jIc903NmGnz=w243-h334-no";

  public static function showDetail($request, $response) {
    global $app;

    if (isset($_GET["id"])) {
      $recipeId = $_GET["id"];

      $result = [];

      $recipe = self::getRecipeById($recipeId);

      if ($recipe != null) {
        $result['recipe'] = json_decode($recipe);

        if ($result['recipe']->ingredients) {
          $result['products'] = [];

          foreach ($result['recipe']->ingredients as $ingredient) {
            $result['products'][] = json_decode(self::getProductByIngredient($ingredient->name));
          }
        }
      }

      return $app->getContainer()->get("renderer")->render($response, 'index.phtml', $result);
    } else if (isset($_GET['ocr'])) {
      $recipe = json_decode(self::getScan());

      if ($recipe != null) {
        $result = [
          'products' => [],
          'scanImage' => 'https://lh3.googleusercontent.com/-DoMllSbJ1AdhsdGGsJz3U0zyzMuG0nQkb59pRaOZRy_pWA6bF_KYhOnsxIzfs8c5jIc903NmGnz=w243-h334-no'
        ];

        foreach ($recipe as $ingredient) {
          if (!empty($ingredient)) $result['products'][] = $ingredient;
        }

        return $app->getContainer()->get("renderer")->render($response, 'index.phtml', $result);
      }
    } else {
      $result = [ 'recipes' => json_decode(self::getRecipesList())];
      return $app->getContainer()->get("renderer")->render($response, 'list.phtml', $result);
    }
  }

  private static function getRecipeById($id) {
    $client = new Client();
    $res = $client->request('GET', str_replace("%id%", $id, self::$urlRecipe), ['content-type' => 'application/json']);
    return $res->getBody()->getContents();
  }

  private static function getProductByIngredient($ingredient) {
    $client = new Client();
    $res = $client->request('GET', str_replace("%ingredient%", urlencode($ingredient), self::$urlProductByIngredient), ['content-type' => 'application/json']);
    return $res->getBody()->getContents();
  }

  private static function getRecipesList() {
    $client = new Client();
    $res = $client->request('GET', self::$urlList, ['content-type' => 'application/json']);
    return $res->getBody()->getContents();
  }

  private static function getScan() {
    $client = new Client();
    $res = $client->request('GET', self::$urlScan, ['content-type' => 'application/json']);
    return $res->getBody()->getContents();
  }
}
