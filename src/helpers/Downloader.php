<?php

namespace Recipes\Helpers;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;

use wapmorgan\UnifiedArchive\UnifiedArchive;
use GuzzleHttp\Promise;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

class Downloader {

  public static function downloadToVariable($url) {
    try {
      $client = new Client();
      $promise = $client->requestAsync('GET', $url, ['decode_content' => false])
      ->then(
        function (ResponseInterface $res) {
          return $res->getBody();
        },
        function (RequestException $e) {
          return false;
        }
      )->wait();
    } catch (Exception $e) {
      return false;
    }
  }
}
