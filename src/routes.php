<?php

// controllers namespace
$controllers = "Recipes\Controllers\\";

$app->get("/", "{$controllers}RecipesController::showDetail");
